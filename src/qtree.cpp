/*
 * qtree.cpp
 *
 *  Created on: Apr 12, 2014
 *      Author: zrt
 */

#include "qtree.h"

qtree::qtree(qtree *par, int lev, double *anch) {
	// TODO Auto-generated constructor stub
//	printf("calling qtree::qtree()\n");
	gids=NULL;
	midids=NULL;
	no=0;
	eid=-1;
	kids=NULL;
	parent=par;
	level=lev;
	data=NULL;
	isleaf=true;
	if (anch==NULL){
		anchor=(double *)malloc(2*sizeof(double));
		anchor[0]=0.0;anchor[1]=0.0;
	}else{
		anchor=anch;
	}
//	printf("level:%d,node:%0lx,anchor:[%1.4f,%1.4f],parent:%0lx\n",level,this,anchor[0],anchor[1],parent);
}

qtree::~qtree() {
	// TODO Auto-generated destructor stub
//	printf("calling qtree::~qtree()\n");
	if (gids!=NULL)
		free(gids);
	if (midids !=NULL)
		free(midids);
	if (kids!=NULL){
		for (int i=0;i<4;i++){
			delete(kids[i]);
		}
		free(kids);
	}
	if (data!=NULL)
		free(data);
	if (anchor!=NULL)
		free(anchor);
}

double qtree::width(){
	double h = pow(0.5,level);
	return h;
}

void qtree::corners(double cor[]){
	double h=width();

	cor[0]=anchor[0];
	cor[2]=anchor[1];

	cor[1]=cor[0]+h;
	cor[3]=cor[2]+h;

	if (cor[1]==1.0)
		cor[1]=1+DBL_EPSILON;

	if (cor[3]==1.0)
		cor[3]=1+DBL_EPSILON;
}

void qtree::create_kids(){
//	printf("calling qtree::create_kids:level:%d,node:%0lx,anchor:[%1.4f,%1.4f]\n",level,this,anchor[0],anchor[1]);
	assert(kids==NULL);

	int i,kidlevel = level+1;
	double kidh = width()*0.5;
	kids=(qtree **)malloc(4*sizeof(qtree *));

	for (i=0;i<4;i++){
		double *kidanchor=(double *)malloc(2*sizeof(double));
		int xd=i%2,yd=i/2;
		kidanchor[0]=anchor[0]+xd*kidh;
		kidanchor[1]=anchor[1]+yd*kidh;
		kids[i]=new qtree(this,kidlevel,kidanchor);
	}

	isleaf=false;
}

void qtree::build_tree_par(char *mid_ids,double **points, int no, int maxPntsPerNode, int maxLevel){
//	printf("qtree::build_tree_par\n");
	int num_threads,blocksize,i;
	size_t size_midid=sizeof(uint64_t)+sizeof(int);
	uint64_t first,last;

	num_threads=get_omp_num_threads();

	qsort_merge_par((void*)mid_ids, (size_t)no, size_midid, mid_id_compare);

	blocksize=(int)floor((double)no/(double)num_threads);

	uint64_t *qdts=(uint64_t*)malloc(num_threads*sizeof(uint64_t));

	#pragma omp parallel for private(i,first,last)
	for (i=0;i<num_threads;i++){
		first=*(uint64_t*)(mid_ids+size_midid*i*blocksize);
		if (i==num_threads){
			last=*(uint64_t*)(mid_ids+size_midid*(no-1));
		}else{
			last=*(uint64_t*)(mid_ids+size_midid*((i+1)*blocksize-1));
		}
		qdts[i]=coarse_filling_qdt(first,last);
	}

	qdt_to_bqtree(qdts, num_threads, maxLevel);

	free(qdts);

	std::vector<qtree *> bqtree_leaves = leaves();
//	printf("bleaves:\n");print_mids(true);

	int n_bleaves=bqtree_leaves.size();//printf("%d\n",n_bleaves);
	int *bleaves_nidx=(int*)malloc(n_bleaves*sizeof(int));
	char **bleaves_mididx=(char**)malloc(n_bleaves*sizeof(char*));

//	printf("1\n");

	#pragma omp parallel for\
		private(i)
	for (i=0;i<n_bleaves;i++){
		bleaves_mididx[i]=bqtree_leaves[i]->points_in_node_morton(mid_ids, no, bleaves_nidx[i]);
	}

	int *cu_bleaves_nidx=(int*)malloc(n_bleaves*sizeof(int));
	memcpy((void *)cu_bleaves_nidx,(const void *)bleaves_nidx,n_bleaves*sizeof(int));
//	printf("2\n");

	genericScan(cu_bleaves_nidx, n_bleaves, sizeof(int), userBinaryOperatorint);


//	for(int k=0;k<n_bleaves;k++){
//		printf("cu_bleaves_nidx[%d]:%d\n",k,cu_bleaves_nidx[k]);
//	}

//	int *lowrange=(int*)malloc((num_threads)*sizeof(int));
	int *upprange=(int*)malloc((num_threads+1)*sizeof(int));
	upprange[0]=-1;

	#pragma omp parallel for private(i)
	for (i=0;i<num_threads;i++){
		int ltarget,utarget;
//		ltarget=i*blocksize;
		utarget=i<(num_threads-1)?(i+1)*blocksize:no;
//		printf("l%d,u%d\n",ltarget,utarget);
//		lowrange[i]=last_floor_bsearch(&ltarget, cu_bleaves_nidx, n_bleaves, sizeof(int), intcompare);
		upprange[i+1]=last_floor_bsearch(&utarget, cu_bleaves_nidx, n_bleaves, sizeof(int), intcompare);
	}

//////	printf("num_threads:%d\n",num_threads);
//	for (i=0;i<num_threads+1;i++){
////		printf("lowrange[%d]:%d \n",i,lowrange[i]);
//		printf("upprange[%d]:%d\n",i,upprange[i]);
//	}
//
//	int oo;
//	std::cin >>  oo;
//
////	assert(range[num_threads]==n_bleaves-1);

	free(cu_bleaves_nidx);

//	int *leaf_sizes=(int*)malloc(num_threads*sizeof(int));

	// insert points
	#pragma omp parallel for private(i)
	for (i=0;i<num_threads;i++){
		for (int k=upprange[i]+1;k<upprange[i+1]+1;k++){
//			printf("workingon:%d!!!!!!!!!!!!!!!!!!!!!!!!!\n",k);
//			printf("%d",bleaves_nidx[k]);


//			int *bleaves_idx=mid_id_fetch_id(bleaves_mididx[k],bleaves_nidx[k]);
//			bqtree_leaves[k]->insert_points(bleaves_idx, points,bleaves_nidx[k], maxPntsPerNode, maxLevel);
			bqtree_leaves[k]->insert_points_morton(bleaves_mididx[k], points,bleaves_nidx[k], maxPntsPerNode, maxLevel);
			free(bleaves_mididx[k]);
//			free(bleaves_idx);
			//can average tree also
//			bqtree_leavs[k]->
		}
	}



//	free(lowrange);
	free(upprange);
	free(bleaves_mididx);
	free(bleaves_nidx);

}

std::vector<qtree*> qtree::build_tree_average_par(char *mid_ids,double **points, int no, int maxPntsPerNode, int maxLevel,user_data &ud){
//	printf("qtree::build_tree_par\n");
	int num_threads,blocksize,i;
	size_t size_midid=sizeof(uint64_t)+sizeof(int);
	uint64_t first,last;

	num_threads=get_omp_num_threads();

	qsort_merge_par((void*)mid_ids, (size_t)no, size_midid, mid_id_compare);

	blocksize=(int)floor((double)no/(double)num_threads);

	uint64_t *qdts=(uint64_t*)malloc(num_threads*sizeof(uint64_t));

	#pragma omp parallel for private(i,first,last)
	for (i=0;i<num_threads;i++){
		first=*(uint64_t*)(mid_ids+size_midid*i*blocksize);
		if (i==num_threads){
			last=*(uint64_t*)(mid_ids+size_midid*(no-1));
		}else{
			last=*(uint64_t*)(mid_ids+size_midid*((i+1)*blocksize-1));
		}
		qdts[i]=coarse_filling_qdt(first,last);
	}

	qdt_to_bqtree(qdts, num_threads, maxLevel);

	free(qdts);

	std::vector<qtree *> bqtree_leaves = leaves();
//	printf("bleaves:\n");print_mids(true);

	int n_bleaves=bqtree_leaves.size();//printf("%d\n",n_bleaves);
	int *bleaves_nidx=(int*)malloc(n_bleaves*sizeof(int));
	char **bleaves_mididx=(char**)malloc(n_bleaves*sizeof(char*));

//	printf("1\n");

	#pragma omp parallel for\
		private(i)
	for (i=0;i<n_bleaves;i++){
		bleaves_mididx[i]=bqtree_leaves[i]->points_in_node_morton(mid_ids, no, bleaves_nidx[i]);
	}

	int *cu_bleaves_nidx=(int*)malloc(n_bleaves*sizeof(int));
	memcpy((void *)cu_bleaves_nidx,(const void *)bleaves_nidx,n_bleaves*sizeof(int));
	//printf("2\n");

	genericScan(cu_bleaves_nidx, n_bleaves, sizeof(int), userBinaryOperatorint);


//	for(int k=0;k<n_bleaves;k++){
//		printf("cu_bleaves_nidx[%d]:%d\n",k,cu_bleaves_nidx[k]);
//	}

//	int *lowrange=(int*)malloc((num_threads)*sizeof(int));
	int *upprange=(int*)malloc((num_threads+1)*sizeof(int));
	upprange[0]=-1;

	#pragma omp parallel for private(i)
	for (i=0;i<num_threads;i++){
		int ltarget,utarget;
//		ltarget=i*blocksize;
		utarget=i<(num_threads-1)?(i+1)*blocksize:no;
//		printf("l%d,u%d\n",ltarget,utarget);
//		lowrange[i]=last_floor_bsearch(&ltarget, cu_bleaves_nidx, n_bleaves, sizeof(int), intcompare);
		upprange[i+1]=last_floor_bsearch(&utarget, cu_bleaves_nidx, n_bleaves, sizeof(int), intcompare);
	}

//////	printf("num_threads:%d\n",num_threads);
//	for (i=0;i<num_threads+1;i++){
////		printf("lowrange[%d]:%d \n",i,lowrange[i]);
//		printf("upprange[%d]:%d\n",i,upprange[i]);
//	}
//
//	int oo;
//	std::cin >>  oo;
//
////	assert(range[num_threads]==n_bleaves-1);

	free(cu_bleaves_nidx);

	int *leaf_sizes=(int*)malloc(num_threads*sizeof(int));
	std::vector<qtree*> *bqtree_subleaves=new std::vector<qtree*>[num_threads];

	// insert points
//	nbody dummy;
	#pragma omp parallel for private(i)
	for (i=0;i<num_threads;i++){

		for (int k=upprange[i]+1;k<upprange[i+1]+1;k++){
//			printf("workingon:%d!!!!!!!!!!!!!!!!!!!!!!!!!\n",k);int fuck;
//			printf("%d",bleaves_nidx[k]);

//			std::cin >> fuck;
//			int *bleaves_idx=mid_id_fetch_id(bleaves_mididx[k],bleaves_nidx[k]);
//			bqtree_leaves[k]->insert_points(bleaves_idx, points,bleaves_nidx[k], maxPntsPerNode, maxLevel);
			bqtree_leaves[k]->insert_points_morton(bleaves_mididx[k], points,bleaves_nidx[k], maxPntsPerNode, maxLevel);
			free(bleaves_mididx[k]);
//			free(bleaves_idx);
			//can average tree also
			std::vector<qtree*> sub_leaves=bqtree_leaves[k]->leaves();
			bqtree_subleaves[i].reserve(bqtree_subleaves[i].size()+sub_leaves.size());
			bqtree_subleaves[i].insert(bqtree_subleaves[i].end(),sub_leaves.begin(),sub_leaves.end());
			for (int p=0;p<sub_leaves.size();p++){
				sub_leaves[p]->average_leaves(ud);
			}
			bqtree_leaves[k]->average_tree_internal();
			//can extract the tree leaves also;
			sub_leaves.clear();
		}
		leaf_sizes[i]=bqtree_subleaves[i].size();
	}
	average_tree_internal_bqtree();

	genericScan(leaf_sizes, num_threads, sizeof(int),userBinaryOperatorint);

	std::vector<qtree*> all_leaves;
	all_leaves.resize(leaf_sizes[num_threads-1]);
	#pragma omp parallel for private(i)
	for (i=0;i<num_threads;i++){
		int begin=i > 0 ? leaf_sizes[i-1]:0;
//		int end=leaf_sizes[i];

		std::copy(bqtree_subleaves[i].begin(),bqtree_subleaves[i].end(),all_leaves.begin()+begin);
	}


	delete [] bqtree_subleaves;
	free(leaf_sizes);
//	free(lowrange);
	free(upprange);
	free(bleaves_mididx);
	free(bleaves_nidx);

	return all_leaves;
}

void qtree::qdt_to_bqtree(uint64_t *qdts, int num_t, int maxLevel){
//	printf("qtree::qdt_to_bqtree\n");
	uint64_t lca;
	int i,level,ich,k,lcalevel;
	static morton_id mid_spec;
	qtree** ancestors;

	ancestors=(qtree**)malloc((maxLevel+1)*sizeof(qtree*));

	for (i=0;i<num_t;i++){
		level=(int)(qdts[i]%((uint64_t)1 << mid_spec.get_lb()));
		if (i==0){
			lcalevel=0;
			create_kids();
			ancestors[0]=this;
		}else{
			lca=mid_spec.last_common_ancestor(qdts[i-1],qdts[i]);
			lcalevel=(int)(lca%((uint64_t)1 << mid_spec.get_lb()));
		}
		for (k=lcalevel+1;k<level;k++){
			ich=mid_spec.get_numbering(qdts[i],k);
			ancestors[k]=ancestors[k-1]->kids[ich];
			ancestors[k]->create_kids();
		}
	}
//	printf("exit qtree::qdt_to_bqtree\n");
}


uint64_t qtree::coarse_filling_qdt(uint64_t first, uint64_t last){
//	printf("qtree::coarse_filling_qdt\n");
	static morton_id mid_spec;
	uint64_t lca,cqdt;
	uint64_t af=0, al=0;
	int f_level, l_level, c_level,level,h_level;

	lca=mid_spec.last_common_ancestor(first,last);
	f_level=(int)(first%((uint64_t)1 << mid_spec.get_lb()));
	l_level=(int)(last%((uint64_t)1 << mid_spec.get_lb()));
	c_level=(int)(lca%((uint64_t)1 << mid_spec.get_lb()));

//	int threads=omp_get_thread_num();
//	int total=get_omp_num_threads();






	h_level=f_level > l_level ? f_level:l_level;
	uint64_t *ans_sib=(uint64_t*)malloc(3*sizeof(uint64_t));
	for (level=c_level+1;level<h_level;level++){
		if (level <= f_level){
			mid_spec.get_ancestor_sibling(first,level,ans_sib);
			for (int i=0;i<3;i++){
				if (ans_sib[i] > first && ans_sib[i] < last){
					if (level < l_level )
						al=mid_spec.get_ancestor(last,level);
					if (level >= l_level || al!=ans_sib[i]){
						cqdt=ans_sib[i];
//						//debug
//						for (int i=0;i<total;i++){
//							if (threads==i){
//								printf("thread:%d,af:\n",i);
//								printf("firt:");mid_spec.print(first);printf("\n");
//								printf("last:");mid_spec.print(last);printf("\n");
//								printf("lcat:");mid_spec.print(lca);printf("\n");
//								for (int k=0;k<3;k++){
//									printf("a[%d]:",k);mid_spec.print(ans_sib[k]);printf("\n");
//								}
//								printf("cqdt:");mid_spec.print(cqdt);printf("\n");
//							}
//						#pragma omp barrier
//						}
//						//enddebug
						free(ans_sib);
						return cqdt;
					}
				}
			}
		}
		if (level <= l_level){
			mid_spec.get_ancestor_sibling(last,level,ans_sib);
			for (int i=0;i<3;i++){
				if (ans_sib[i] > first && ans_sib[i] < last){
					if (level < f_level )
						af=mid_spec.get_ancestor(last,level);
					if (level >= f_level || af!=ans_sib[i]){
						cqdt=ans_sib[i];
//						//debug
//						for (int i=0;i<total;i++){
//							if (threads==i){
//								printf("thread:%d,al:\n",i);
//								printf("firt:");mid_spec.print(first);printf("\n");
//								printf("last:");mid_spec.print(last);printf("\n");
//								printf("lcat:");mid_spec.print(lca);printf("\n");
//								for (int k=0;k<3;k++){
//									printf("a[%d]:",k);mid_spec.print(ans_sib[k]);printf("\n");
//								}
//								printf("cqdt:");mid_spec.print(cqdt);printf("\n");
//							}
//						#pragma omp barrier
//						}
//						//enddebug
						free(ans_sib);
						return cqdt;
					}
				}
			}
		}
	}
	free(ans_sib);

	return first;
}


void qtree::insert_points(int *gids_in,double **points,int nn,int maxPntsPerNode,int maxLevel){
	if (nn==0)
		return;
	int i;

	int *newgids=NULL,newno=0;
	if (isleaf){
		newgids=(int *)unique_merge_sorted((void*)gids,no,(void*)gids_in,(size_t)nn,sizeof(int),intcompare,newno);

		if (gids!=NULL){
			free(gids);gids=NULL;no=0;
		}

		if (newno <= maxPntsPerNode){
			gids=newgids;no=newno;
			return;
		}

		create_kids();
	}else{
		newgids=(int *)unique_merge_sorted((void*)gids,no,(void*)gids_in,(size_t)nn,sizeof(int),intcompare,newno);
	}

	int *kidgids,nidx;
	for (i=0;i<4;i++){
		nidx=0;
		kidgids=kids[i]->points_in_node(points,newgids,newno,nidx);
		kids[i]->insert_points(kidgids,points,nidx,maxPntsPerNode,maxLevel);
		if (kidgids!=NULL){
			free(kidgids);kidgids=NULL;
		}
	}

	if (newgids!=NULL){
		free(newgids);
	}
}

void qtree::insert_points_morton(char *mid_id_in, double **points, int nn, int maxPntsPerNode, int maxLevel){
//	printf("qtree::insert_points_morton\n");
	if (nn==0)
		return;
	int i;
	size_t size_midid=sizeof(uint64_t)+sizeof(int);


	int newno=0;
	char *newmidids=NULL;
	if (isleaf){
		newmidids=(char *)unique_merge_sorted((void*)midids,no,(void*)mid_id_in,nn,size_midid,mid_id_compare,newno);
//		//********************************
//		int *newids=mid_id_fetch_id(newmidids,newno);
//		morton_id midspec;
//
//		double cor[4];
//		corners(cor);
//		printf("level%d:[xmin,xmax,ymin,ymax]=[%f,%f,%f,%f]\n",level,cor[0],cor[1],cor[2],cor[3]);
//
//		for (i=0;i<nn;i++){
//			printf("point[%d]:[%f,%f]\n",newids[i],points[newids[i]][0],points[newids[i]][1]);
////			printf("mid:");uint64_t pointmid=*(uint64_t*)(mid_id_in+size_midid*i);
//
//
////			midspec.print(pointmid);
////			printf("\n");
//
////			double *point=midspec.mid2double(pointmid);
//
////			printf("point:[%e,%e]\n",point[0],point[1]);
//
////			free(point);
//
//		}
//		printf("points!!!!!!!!!!!!!!!!!!\n");

//
//		free(newids);
//		////////////////////////////


		if (gids!=NULL){
			free(gids);gids=NULL;no=0;
		}
		if (midids!=NULL){
			free(midids);midids=NULL;
		}

		if (newno <= maxPntsPerNode){
			gids=mid_id_fetch_id(newmidids,newno);
			midids=newmidids;no=newno;
			return;
		}

		create_kids();
	}else{
		newmidids=(char *)unique_merge_sorted((void*)midids,no,(void*)mid_id_in,nn,size_midid,mid_id_compare,newno);
	}

	int nidx;
	char *kidmidids;
	for (i=0;i<4;i++){
		nidx=0;
		kidmidids=kids[i]->points_in_node_morton(newmidids,newno,nidx);
		kids[i]->insert_points_morton(kidmidids,points,nidx,maxPntsPerNode,maxLevel);
		if (kidmidids!=NULL){
			free(kidmidids);kidmidids=NULL;
		}
	}

	if (midids!=NULL){
		free(newmidids);
	}
//	printf("exit qtree::insert_points_morton\n");
}

char* qtree::points_in_node_morton(char *mid_id_in, int nn, int &nidx){//output mid_ids in nodes
//	printf("qtree::points_in_node_morton\n");
//	printf("nn:%d\n",nn);
	int threads=omp_get_thread_num();

	size_t size_midid=(sizeof(uint64_t)+sizeof(int));
	static morton_id mid_spec;
	uint64_t nodemid,upperbound,lowerbound;
	char *fch,*lch;

	nodemid=mid_spec.id(level,anchor);

	lowerbound=mid_spec.get_ch_lower(nodemid,level);
	upperbound=mid_spec.get_ch_upper(nodemid,level);

////	mid_spec.print(upperbound);printf("\n");
//	printf("mid:\n");mid_spec.print(nodemid);printf("\n");
//	printf("low:\n");mid_spec.print(lowerbound);printf("\n");
//	printf("upp:\n");mid_spec.print(upperbound);printf("\n");
//
//	for (int i=0;i<nn;i++){
//		printf("point[%d]:\n",i);
//		uint64_t curmid=*(uint64_t*)(mid_id_in+i*size_midid);
//		printf("%d",i);mid_spec.print(curmid);printf("\n");
//	}


	fch=(char*)ceil_bsearch((const void*)&lowerbound, (const void*) mid_id_in, (size_t)nn, size_midid, mid_id_compare);
	lch=(char*)floor_bsearch((const void*)&upperbound, (const void*) mid_id_in, (size_t)nn, size_midid, mid_id_compare);

//	uint64_t first=0,last=0;
//	if (fch!=NULL)	first=*(uint64_t*)fch;
//	if (lch!=NULL)	last=*(uint64_t*)lch;
//
//	printf("first at %lx:\n",fch);mid_spec.print(first);printf("\n");
//	printf("last at %lx:\n",lch);mid_spec.print(last);printf("\n");

	if (fch==NULL || lch==NULL || fch > lch){
		nidx=0;
	}else{
		nidx=(lch-fch)/size_midid+1;
//		printf("thread:%d,nidx:%d\n",threads,nidx);
	}

//	printf("nidx:%d\n",nidx);
	char *mididx;
	if (nidx!=0){
		mididx=(char*)malloc(nidx*size_midid);//printf("thread:%d,mididx:%lu\n",threads,mididx);
		memcpy((void*)mididx,(const void*)fch,nidx*size_midid);//printf("????\n");
	}else{
		mididx = NULL;
	}
//	int fangp;
//	std::cin >> fangp;
//	printf("exit qtree::points_in_node_morton\n");
	return mididx;

}

int* qtree::points_in_node(double **points,int *gids_in,int nn, int &nidx){
	int i,gid,iidx;
	int *idx=(int *)malloc(nn*sizeof(int));
	double cor[4];
	corners(cor);

	for (i=0,iidx=0;i<nn;i++){
		gid=gids_in[i];
		if (points[gid][0]>=cor[0] && points[gid][0]<cor[1] && points[gid][1]>=cor[2] && points[gid][1]<cor[3]){
			idx[iidx]=gid;iidx++;
		}
	}
	nidx=iidx;

	if (nidx==0){
		free(idx);idx=NULL;
	}else if (nidx<nn){
		idx=(int *)realloc(idx,nidx*sizeof(int));
	}

	return idx;
}

//void qtree::preorder(void (*visit)(void*),bool (*prune)(void*)=NULL,void* user_data=NULL){
//	bool doPrune=false;
//
//	if (prune!=NULL)
//		doPrune=prune(user_data);
//
//	visit(user_data);
//	if (isleaf==false && doPrune==false){
//		for (int i=0;i<4;i++){
//			kids[i]->preorder(visit,prune,user_data);
//		}
//	}
//}

//void qtree::postorder(void (*visit)(void*),bool (*prune)(void*)=NULL,void* user_data=NULL){
//	bool doPrune=false;
//
//	if (prune!=NULL)
//		doPrune=prune(user_data);
//
//	if (isleaf==false && doPrune==false){
//		for (int i=0;i<4;i++){
//			kids[i]->preorder(visit,prune,user_data);
//		}
//	}
//	visit(user_data);
//}

std::vector<qtree*> qtree::leaves(){
	std::vector<qtree*> list;

	get_leaves(list);

	return list;
}

void qtree::get_leaves(std::vector<qtree*> &list){
	if (isleaf==true){
		list.push_back(this);
	}else{
		for (int i=0;i<4;i++)
			kids[i]->get_leaves(list);
	}
}

std::vector<qtree*> qtree::nodes(){
	std::vector<qtree*> list;

	get_nodes(list);

	return list;
}

void qtree::get_nodes(std::vector<qtree*> &list){
	list.push_back(this);
	if (isleaf==false){
		for (int i=0;i<4;i++)
			kids[i]->get_nodes(list);
	}
}

void qtree::get_eid_par(std::vector<qtree*> &list){
	int i,length;
	length=list.size();

	#pragma omp parallel for schedule(static)\
			private(i)
	for (i=0;i<length;i++){
		list[i]->eid=i;
	}
}

void qtree::print(){
	printf("node at level %d: anchor:[%1.4f,%1.4f]\n",level,anchor[0],anchor[1]);
	if (isleaf)
		return;
	for (int i=0;i<4;i++)
		kids[i]->print();
}

void qtree::print_mids(bool leaves_only){
	static morton_id mid;

	if (leaves_only==false || isleaf==true){
		printf("mid:");
		uint64_t id=mid.id(level,anchor);
		mid.print(id);
		printf(": %20lu at level %2d: anchor:[%1.4f,%1.4f]\n",id,level,anchor[0],anchor[1]);
	}

	if (isleaf)
		return;

	for (int i=0;i<4;i++)
		kids[i]->print_mids(leaves_only);
}

int qtree::depth(){
	int depth=0;

	get_depth(depth);
	return	depth;
}

void qtree::get_depth(int &depth){
	depth=level > depth ? level:depth;
	if (isleaf==true){
		return;
	}else{
		for (int i=0;i<4;i++){
			kids[i]->get_depth(depth);
		}
	}
}

void qtree::average_tree_internal(){
//	printf("nbody::average_tree_internal()\n");
	if (isleaf)
		return;

	int i;

	for (i=0;i<4;i++){
		kids[i]->average_tree_internal();
	}
	average_internal();
}

void qtree::average_tree_internal_bqtree(){
//	printf("nbody::average_tree_internal()\n");
	if (isleaf || data!=NULL )
		return;

	int i;

	for (i=0;i<4;i++){
		kids[i]->average_tree_internal_bqtree();
	}
	average_internal();
}

void qtree::average_leaves(user_data &ud){
//	printf("nbody::average_leaves()\n");
//	printf("level:%d\n",level);
	if (!isleaf)
		return;
	double x_a=0.0,y_a=0.0,d_a=0.0;
	int i,np;
	np=no;

	if (gids!=NULL){
		for (i=0;i<np;i++){
			d_a+=ud.densities[gids[i]];
			x_a+=ud.points[gids[i]][0]*ud.densities[gids[i]];
			y_a+=ud.points[gids[i]][1]*ud.densities[gids[i]];
		}
		if (d_a==0.0){
			x_a=0.0;
			y_a=0.0;
		}else{
			x_a=x_a/d_a;
			y_a=y_a/d_a;
		}
	}

	if (data==NULL){
		data=(double *)malloc(3*sizeof(double));
	}
	data[0]=x_a;
	data[1]=y_a;
	data[2]=d_a;
}

void qtree::average_internal(){
//	printf("nbody::average_internal()\n");
	if (isleaf)
		return;
	double x_a=0.0,y_a=0.0,d_a=0.0;
	int i;

	for (i=0;i<4;i++){
		d_a+=kids[i]->data[2];
		x_a+=kids[i]->data[0]*kids[i]->data[2];
		y_a+=kids[i]->data[1]*kids[i]->data[2];
	}

	if (d_a==0.0){
		x_a=0.0;
		y_a=0.0;
	}else{
		x_a=x_a/d_a;
		y_a=y_a/d_a;
	}

	if (data==NULL){
		data=(double *)malloc(3*sizeof(double));
	}
	data[0]=x_a;
	data[1]=y_a;
	data[2]=d_a;
}

