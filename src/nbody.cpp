/*
 * nbody.cpp
 *
 *  Created on: Apr 16, 2014
 *      Author: zrt
 */
#include "nbody.h"

//user_data::user_data(double **pnts, double *den, int nn):points(pnts),densities(den),no(nn){
////	printf("user_data::user_data()\n");
//
//}
//
//user_data::~user_data(){
////	printf("user_data::~user_data()\n");
//}

euler_tour::euler_tour(std::vector<qtree*> &nodes, std::vector<int> &subsize):I(nodes.size(),0),O(nodes.size(),0){
	int i,length=nodes.size();

	#pragma omp parallel for private(i)
	for (i=0;i<length;i++){
		I[i]=2*(nodes[i]->eid)-nodes[i]->level;
		O[i]=I[i]+2*subsize[i];
	}
}

euler_tour::~euler_tour(){
	I.clear();
	O.clear();
}


nbody::nbody():potential(NULL),nop(0),root(NULL),gids(NULL){
//	printf("nbody::nbody()\n");
}


nbody::~nbody(){
//	printf("nbody::~nbody()\n");
	if (potential!=NULL)
		free(potential);
	if (root!=NULL){
		delete(root);
	}
	if (gids!=NULL)
		free(gids);
}

void nbody::run_nbody(double **points, double *densities, int no, int maxPntsPerNode, int maxLevel){
//	printf("nbody::run_nbody()\n");
	int i;

	if (points==NULL){
//		selftest();
		return;
	}

	selfclear();

	nop=no;

	for (i=0;i<no;i++){
		assert(densities[i]>=0.0);
	}

	gids=(int *)malloc(no*sizeof(int));
	for (i=0;i<no;i++){
		gids[i]=i;
	}

	root =new qtree;

	root->insert_points(gids,points,no,maxPntsPerNode,maxLevel);

	user_data ud(points,densities,no);

	std::vector<qtree*> leaves=root->leaves();

	// average
	qtree *leaf=NULL;
	for (i=0;i<leaves.size();i++){
		leaf=leaves[i];
		average_leaves(leaf,ud);
	}

	average_tree_internal(root);

	// evaluate
	potential=(double *)malloc(no*sizeof(double));
	for (i=0;i<leaves.size();i++){
		leaf=leaves[i];
		if (leaf->gids!=NULL)
			evaluate(leaf,ud);
	}

}

void nbody::run_nbody_par(double **points, double *densities, int no, int maxPntsPerNode, int maxLevel){
	int i;

	if (points==NULL){
//		selftest();
		return;
	}

	selfclear();

	nop=no;

	#pragma omp parallel for private(i)
	for (i=0;i<no;i++){
		assert(densities[i]>=0.0);
	}

	user_data ud(points,densities,no);

	size_t size_midid=sizeof(uint64_t)+sizeof(int);

	char *mid_ids=(char*)malloc(no*size_midid);

	init_mid_id_par(points, mid_ids, no, maxLevel);

//	/////////////////////////
//	static morton_id midspecc;
//	int *index=mid_id_fetch_id(mid_ids,no);
//
//	for (int k=0;k<no;k++){
//		uint64_t midd=*(uint64_t*)(mid_ids+size_midid*k);
//		printf("%d:",k);midspecc.print(midd);printf("\n");
//
//		double *coord=midspecc.mid2double(midd);
//
//		printf("mid2double[%d]:",k);printf("[%e,%e]\n",coord);
//		printf("point[%d]",index[k]);printf("[%e,%e]\n",points[index[k]][0],points[index[k]][1]);
//
//		free(coord);
//	}
//	free(index);
//	int fangp;
//	std::cin >> fangp;
////////////////////////////


	qsort_merge_par((void *)mid_ids, (size_t)no, size_midid, mid_id_compare);

//	/////////////////////////
////	static morton_id midspecc;
//	index=mid_id_fetch_id(mid_ids,no);
//
//	for (int k=0;k<no;k++){
//		uint64_t midd=*(uint64_t*)(mid_ids+size_midid*k);
//		printf("%d:",k);midspecc.print(midd);printf("\n");
//
//		double *coord=midspecc.mid2double(midd);
//
//		printf("mid2double[%d]:",k);printf("[%e,%e]\n",coord);
//		printf("point[%d]",index[k]);printf("[%e,%e]\n",points[index[k]][0],points[index[k]][1]);
//
//		free(coord);
//	}
//	free(index);
////	int fangp;
//	std::cin >> fangp;
////////////////////////////


	gids=mid_id_fetch_id(mid_ids,no);

	root =new qtree;

	//purely build
//	root->build_tree_par(mid_ids,points,no,maxPntsPerNode,maxLevel);
	//build and average
	std::vector<qtree*> leaves=root->build_tree_average_par(mid_ids,points,no,maxPntsPerNode,maxLevel,ud);


//	root->insert_points(gids,points,no,maxPntsPerNode,maxLevel);



//	std::vector<qtree*> leaves=root->leaves();
//	std::vector<qtree*> nodes=root->nodes();

	int length=leaves.size();
	qtree *leaf=NULL;
////use euler_tour
//	// build euler_tour
//	root->get_eid_par(nodes);
//	std::vector<int> subsize=get_subsize_par(nodes);
//	euler_tour eutor(nodes,subsize);
//
//	subsize.clear();
//
//	// average
//
//	//	#pragma omp parallel for private(i,leaf) shared(leaves,ud)
//	for (i=0;i<length;i++){
//		leaf=leaves[i];
//		average_leaves(leaf,ud);
//	}
//
//	average_tree_internal_par(nodes,eutor);
////end use euler_tour
	// evaluate
	potential=(double *)malloc(no*sizeof(double));
	length=leaves.size();
	#pragma omp parallel for schedule(dynamic,20)\
			private(i,leaf) shared(ud,leaves)
	for (i=0;i<length;i++){
		leaf=leaves[i];
		if (leaf->gids!=NULL)
			evaluate(leaf,ud);
	}
}

void nbody::init_mid_id_par(double **points, char *mid_ids, int no, int level){
	int i;
	uint64_t mid;
	size_t h=sizeof(uint64_t)+sizeof(int);
	static morton_id midspec;

//	#pragma omp parallel for private(i)
	for (i=0;i<no;i++){
		uint64_t utemp=midspec.id(level,points[i]);
		int itemp=i;
		memcpy((void*)(mid_ids+i*h),&utemp,sizeof(uint64_t));
		memcpy((void*)(mid_ids+i*h+sizeof(uint64_t)),&itemp,sizeof(int));
//		*(uint64_t*)(mid_ids+i*h)=midspec.id(level,points[i]);
//		*(int*)(mid_ids+i*h+sizeof(int))=i;
	}
}

void nbody::average_tree_internal_par(std::vector<qtree*> &nodes,euler_tour &eutor){
	int i, length=nodes.size();

	double *Scan=(double *)malloc(length*6*sizeof(double));
	#pragma omp parallel for private(i) shared(length)
	for (i=0;i<length;i++){
		if (nodes[i]->isleaf){
			Scan[eutor.I[i]*3]=nodes[i]->data[0]*nodes[i]->data[2];
			Scan[eutor.I[i]*3+1]=nodes[i]->data[1]*nodes[i]->data[2];
			Scan[eutor.I[i]*3+2]=nodes[i]->data[2];
		}
	}

	genericScan((void *)Scan, length*2, sizeof(double)*3,userBinaryOperatordouble3d);

	#pragma omp parallel for private(i) shared(length)
	for (i=0;i<length;i++){
		if (!nodes[i]->isleaf){
			if (nodes[i]->data==NULL){
				nodes[i]->data=(double*)malloc(3*sizeof(double));
			}
			nodes[i]->data[2]=Scan[eutor.O[i]*3+2]-Scan[eutor.I[i]*3+2];
			if (nodes[i]->data[2]==0.0){
				nodes[i]->data[0]=0.0;
				nodes[i]->data[1]=0.0;
			}else{
				nodes[i]->data[0]=Scan[eutor.O[i]*3]-Scan[eutor.I[i]*3];
				nodes[i]->data[1]=Scan[eutor.O[i]*3+1]-Scan[eutor.I[i]*3+1];
				nodes[i]->data[0]=nodes[i]->data[0]/nodes[i]->data[2];
				nodes[i]->data[1]=nodes[i]->data[1]/nodes[i]->data[2];
			}
		}
	}
	free(Scan);
}



std::vector<int> nbody::get_subsize_par(std::vector<qtree*> &nodes){
	int i,length=nodes.size();
	qtree* rmd;

	assert(length>0);

	std::vector<int> subsize(length,0);

	#pragma omp parallel for schedule (dynamic,100)\
			private(i,rmd) shared(length)
		for (i=0;i<length;i++){
			rmd=right_most_descendant(nodes[i]);
			subsize[i]=rmd->eid-nodes[i]->eid;
		}
	return subsize;
}

qtree* nbody::right_most_descendant(qtree *node){
	qtree *rmd=node;

	while (!rmd->isleaf){
		rmd=rmd->kids[3];
	}

	return rmd;
}

void nbody::average_tree_internal(qtree* node){
//	printf("nbody::average_tree_internal()\n");
	if (node->isleaf)
		return;

	int i;

	for (i=0;i<4;i++){
		average_tree_internal(node->kids[i]);
	}
	average_internal(node);
}

void nbody::average_tree_internal_bqtree(qtree* node){
//	printf("nbody::average_tree_internal()\n");
	if (node->isleaf || node->data!=NULL )
		return;

	int i;

	for (i=0;i<4;i++){
		average_tree_internal_bqtree(node->kids[i]);
	}
	average_internal(node);
}

void nbody::average_leaves(qtree* node, user_data &ud){
//	printf("nbody::average_leaves()\n");
	if (!node->isleaf)
		return;
	double x_a=0.0,y_a=0.0,d_a=0.0;
	int i,np;
	np=node->no;

	if (node->gids!=NULL){
		for (i=0;i<np;i++){
			d_a+=ud.densities[node->gids[i]];
			x_a+=ud.points[node->gids[i]][0]*ud.densities[node->gids[i]];
			y_a+=ud.points[node->gids[i]][1]*ud.densities[node->gids[i]];
		}
		if (d_a==0.0){
			x_a=0.0;
			y_a=0.0;
		}else{
			x_a=x_a/d_a;
			y_a=y_a/d_a;
		}
	}

	if (node->data==NULL){
		node->data=(double *)malloc(3*sizeof(double));
	}
	node->data[0]=x_a;
	node->data[1]=y_a;
	node->data[2]=d_a;
}

void nbody::average_internal(qtree* node){
//	printf("nbody::average_internal()\n");
	if (node->isleaf)
		return;
	double x_a=0.0,y_a=0.0,d_a=0.0;
	int i;

	for (i=0;i<4;i++){
		d_a+=node->kids[i]->data[2];
		x_a+=node->kids[i]->data[0]*node->kids[i]->data[2];
		y_a+=node->kids[i]->data[1]*node->kids[i]->data[2];
	}

	if (d_a==0.0){
		x_a=0.0;
		y_a=0.0;
	}else{
		x_a=x_a/d_a;
		y_a=y_a/d_a;
	}

	if (node->data==NULL){
		node->data=(double *)malloc(3*sizeof(double));
	}
	node->data[0]=x_a;
	node->data[1]=y_a;
	node->data[2]=d_a;
}

void nbody::evaluate(qtree *leaf, user_data &ud){
//	printf("nbody::evaluate()\n");
	if (leaf->gids==NULL)
		return;

	int i;

	double **trg=(double **)fetch_sub_id( (void *)ud.points, ud.no, sizeof(double *), leaf->gids, leaf->no);
	double *pot=(double *)malloc(leaf->no*sizeof(double));
	for (i=0;i<leaf->no;i++){
		pot[i]=0;
	}

	get_evaluate(leaf,root,ud,trg,pot);

	for (i=0;i<leaf->no;i++){
		potential[leaf->gids[i]]=pot[i];
	}

	free(trg);trg=NULL;
	free(pot);pot=NULL;
}

void nbody::get_evaluate(qtree *leaf, qtree *node, user_data &ud, double **trg, double *pot){
//	printf("nbody::get_evaluate()\n");
	if (node->isleaf){
		if (node->gids!=NULL){
//			printf("if?\n");
			double **src=(double**)fetch_sub_id((void *)ud.points,ud.no,sizeof(double*),node->gids,node->no);
			double *den=(double *)fetch_sub_id((void *)ud.densities,ud.no,sizeof(double),node->gids,node->no);
//			printf("if done!\n");

			direct_leaf(trg,leaf->no,src,den,node->no,pot);

			free(src);src=NULL;
			free(den);den=NULL;
		}
		return;
	}

	if (well_separated(node,leaf)==true){
		direct_internal(trg,leaf->no,node->data,pot);
	}else{
		for (int i=0;i<4;i++){
			get_evaluate(leaf,node->kids[i],ud,trg,pot);
		}
	}

}

bool nbody::well_separated(qtree* source, qtree *target){
//	printf("nbody::well_sperated()\n");
	double s_cor[4],t_cor[4],h;

	source->corners(s_cor);
	target->corners(t_cor);
	h=source->width();

	s_cor[0]-=h;s_cor[1]+=h;s_cor[2]-=h;s_cor[3]+=h;

	bool flagx,flagy;
	flagx = t_cor[1]>s_cor[0] && t_cor[0]<s_cor[1];
	flagy = t_cor[3]>s_cor[2] && t_cor[2]<s_cor[3];

//	printf("exiting nbody::well_sperated()\n");
	if (flagx && flagy)
		return false;
	else
		return true;
}

void nbody::direct_leaf(double **trg,int nt,double **src,double *den,int ns,double *pot){
//	printf("nbody::direct_leaf()\n");
	int i,j;
	double rx,ry,r,g;

	for (i=0;i<nt;i++){//can be parallelized easily
		double sum=0;
		for (j=0;j<ns;j++){//need to be parallelized
			rx = trg[i][0]-src[j][0];
			ry = trg[i][1]-src[j][1];
			r = sqrt(rx*rx+ry*ry);

			g = r==0.0 ? 0.0:-log (r);

			sum+=g*den[j];
		}
		pot[i]+=0.5*ivPI*sum;
	}
}

void nbody::direct_internal(double **trg,int nt,double *data,double *pot){
//	printf("nbody::direct_internal()\n");
	int i;
	double rx,ry,r,g;

	for (i=0;i<nt;i++){//can be parallelized easily
		rx = trg[i][0]-data[0];
		ry = trg[i][1]-data[1];
		r = sqrt(rx*rx+ry*ry);

		g = r==0.0 ? 0.0:-log (r);

		pot[i]+=0.5*ivPI*g*data[2];
	}

}

void nbody::selfclear(){
//	printf("nbody::selfclear\n");
	if (potential!=NULL){
		free(potential);potential=NULL;
	}
	if (root!=NULL){
		delete(root);root=NULL;
	}
	if (gids!=NULL){
		free(gids);gids=NULL;
	}
}

//void nbody::selftest(){
//	int n=1 << 6;
//	int maxPntsPerNode=10;
//	int maxLevel=20;
//
//}
