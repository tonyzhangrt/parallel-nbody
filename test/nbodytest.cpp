/*
 * nbodytest.cpp
 *
 *  Created on: Apr 18, 2014
 *      Author: zrt
 */

#include <cfloat>
#include <cstdlib>
#include"../src/nbody.h"

int main( int argc, char **argv){
	int no,maxPntsPerNode,maxLevel,i,power,display;
	if(argc>1)
		power=atoi(argv[1]);
	else
		power=8;
	if (argc>2)
		display=1;
	else
		display=0;

	no=1 << power;
	maxPntsPerNode=10;
	maxLevel=20;

	nbody result;

	double **pnts=(double**)malloc(no*sizeof(double *));
	for (i=0;i<no;i++){
		pnts[i]=(double*)malloc(2*sizeof(double));
//		pnts[i][0]=((double)(i%31))/31.0;
//		pnts[i][1]=((double)(i%23))/23.0;
		pnts[i][0]= (double)(rand() % ((uint32_t)1<<31))/(double)((uint32_t)1<<31);
		pnts[i][1]= (double)(rand() % ((uint32_t)1<<31))/(double)((uint32_t)1<<31);
	}

	double *den=(double*)malloc(no*sizeof(double));
	for (i=0;i<no;i++){
//		den[i]=((double)(i%17))/(17.0*no)+DBL_EPSILON;
		den[i]=(double)(rand() % ((uint32_t)1<<31))/(double)((uint32_t)1<<31)/no+DBL_EPSILON;
	}

	double start_time=omp_get_wtime();
	result.run_nbody_par(pnts, den, no, maxPntsPerNode, maxLevel);
	double timepast=omp_get_wtime()-start_time;

	if (display!=0){
		result.root->print_mids();

		for(i=0;i<no;i++){
			printf("point:[%1.4f,%1.4f],density:%e,potential:%e\n",pnts[i][0],pnts[i][1],den[i],result.potential[i]);
		}
	}

	printf("Run %d points for %f\n",no,timepast);

	for (i=0;i<no;i++){
		free(pnts[i]);
	}
	free(pnts);
	free(den);
}



